// ==UserScript==
// @name         WPPS-DASH
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @description  WPPS Dasboard that displays most used plugins by WPPS Team.
// @author       gtolmac
// @match        *://*/*/wp-admin/*
// @match        *://*/wp-admin/*
// @grant        GM_addStyle
// @require https://code.jquery.com/jquery-3.4.1.min.js
// @require https://raw.githubusercontent.com/gtolmac/wpps-dash/dev/wpps-dash.user.js
// @updateURL   https://raw.githubusercontent.com/gtolmac/wpps-dash/dev/wpps-dash.meta.js
// @downloadURL https://raw.githubusercontent.com/gtolmac/wpps-dash/dev/wpps-dash.user.js
// ==/UserScript==

(function () {
    'use strict';

    // Code for userscript starts here
    $(document).ready(function () {

    // Get and show URL
    var location = window.location.href;
    console.log('Current URL: ' + location);

    // remove some stuff globally on load
    $('.button').removeAttr('data-slug');
    $('.plugins-popular-tags-wrapper').detach();

    // global variables
    var isJPG = true;
    var img2Loaded = false;

    // functions

    if (window.location.href.indexOf('plugin-install.php?s=') > 0) {
        console.log('plugin install search called');
        $(document).ready(function () {
            // var cardGuttenberg = $('.plugin-card-gutenberg').find('.install-now').attr('href');
            // window.location.href = cardGuttenberg;
            var pluginName = sessionStorage.getItem("pluginName"); 
            console.log(pluginName);
            var cardLink = $('.plugin-card-' + pluginName).find('.install-now').attr('href');
            console.log(cardLink);
            window.location.href = cardLink;
        })
    }

    else if (window.location.href.indexOf('plugin-install.php') > 0) {
        console.log('plugin install dash called');

            // var cardGuttenberg = $('.plugin-card-gutenberg').find('.install-now').attr('href');
            // window.location.href = cardGuttenberg;
    
            // Dashboard Takeover
            $('.wp-heading-inline').text('Add Plugins (WPPS)');
            $('.action-links').find('.open-plugin-details-modal').detach();
            $('.plugin-install-tab-featured').find('.plugin-card-bottom').detach();
    
            Inject('autoptimize', 'akismet');
            Inject('classic-editor', 'classic-editor');
            Inject('fast-velocity-minify', 'jetpack');
            Inject('ewww-image-optimizer', 'bbpress');
            Inject('wp-smushit', 'gutenberg');
            Inject('ga-google-analytics', 'buddypress');
            Inject('wp-mail-smtp', 'health-check');
    
    
    
    
        function Inject(pluginName, pluginToChange) {
            $('.plugin-card-' + pluginToChange).find('.open-plugin-details-modal').text(pluginName);
            $('.plugin-card-' + pluginToChange).find('.desc').text('WPPS ' + pluginName + ' Injected');
            $('.plugin-install-tab-featured').find('.button').text('Install Now');
    
            var img = 'https://ps.w.org/' + pluginName + '/assets/icon-128x128.jpg';

            ImageExist(pluginName, pluginToChange, img);
            //Bind(pluginName, pluginToChange, img3);
        }

        function Bind(pluginName, pluginToChange, img){
            var imgTag = '<img src="' + img + '" class="plugin-icon" alt="">';

            $('.plugin-card-' + pluginToChange).find('.open-plugin-details-modal').append(imgTag);
            
            // try to unbind any events from the button
            $('.plugin-card-' + pluginToChange).find('.button').unbind();
            $('.plugin-card-' + pluginToChange).find('.button').off();
    
            $('.plugin-card-' + pluginToChange).find('.button').attr('href', '/wp-admin/plugin-install.php?s=' + pluginName + '&tab=search&type=term');
            $('.plugin-card-' + pluginToChange).find('.button').click(function(){
                sessionStorage.removeItem('pluginName');
                sessionStorage.setItem("pluginName", pluginName);
                window.location.href = '/wpwp/wp-admin/plugin-install.php?s=' + pluginName + '&tab=search&type=term';
            });
        }

        function ImageExist(pluginName, pluginToChange, url) {
            var img2 = new Image();
            img2.src = url;   
            var newurl = '';
            img2.onload = function() {
                console.log('image width: ' + img2.width + ' image height: ' + img2.height);
                if (img2.height != 0 && img2.width != 0) {
                    console.log('returning true');
                    newurl = 'https://ps.w.org/' + pluginName + '/assets/icon-128x128.jpg';
                    Bind(pluginName, pluginToChange, newurl);
                    return;
                }
            }   
            img2.onerror = function() {
                console.log('image width: ' + img2.width + ' image height: ' + img2.height);
                if (img2.height == 0 && img2.width == 0) {
                    console.log('returning false');
                    newurl = 'https://ps.w.org/' + pluginName + '/assets/icon-128x128.png';
                    Bind(pluginName, pluginToChange, newurl);
                    return;
                }
            }
        }
    }

    else if (window.location.href.indexOf('action=install-plugin') > 0) {
        console.log('plugin install called');
    $(document).ready(function(){
            var cardPluginName = $('.button-primary').attr('href');
            window.location.href = cardPluginName;
        })
    }

})
    

})();
